<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta content='IE=edge' http-equiv='X-UA-Compatible'/>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'/>
    <meta content='' name='author'/>
    <meta content='' name='description'/>
    <meta content='' name='keywords'/>
    <link href='css/bootstrap.min.css' media='all' rel='stylesheet' type='text/css'/>
    <link href='css/font-awesome.min.css' media='all' rel='stylesheet' type='text/css'/>
    <link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
    <link rel="shortcut icon" href="img/aco.ico"/>
    <script src="https://kit.fontawesome.com/bf4d0ad315.js" crossorigin="anonymous"></script>
    <title>Login - Administrador</title>
</head>
  <body>
    <div class='wrapper'>
      <aside class="sidebar-left navbar navbar-inverse">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home.php" style="height: 75px;">
             <img src="img/aco-logo-white-50.png" alt="logo-book-with-mouse-clicker">
          </a>
        </div>
        <nav class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <li><a href="home.php"><i class="fas fa-home"></i> <span>INICIO</span></a></li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Cadastre-se</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="cad-bib.php"><i class="fa fa-circle-o"></i> Biblioteca</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fas fa-sign-in-alt"></i> <span>Login</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="adm-login.php"><i class="fa fa-circle-o"></i> Administrador</a></li>
                <li><a href="bib-login.php"><i class="fa fa-circle-o"></i> Biblioteca</a></li>
              </ul>
            </li>
            <li><a href="listar-bib.php"><i class="fas fa-book"></i> <span>Bibliotecas</span></a></li>
          </ul>
        </nav>
      </aside>
      <div class='content container-fluid'>
        <div class='row'>
            <div class='col-md-2 col-md-offset-5'>
                    <br><h1 class="title">Login</h1>
                    <form action="adm-verif.php" method="POST">
                    <input type="hidden" name="nivelADM" value="1">
                        <div class="form-group">
                        <label class=>Usuário:</label>
                        <input type="text" name="loginADM" id="loginADM" class="form-control" style="width: 200px" autofocus required>
                        <label class="2">Senha:</label>
                        <input type="password" name="senhaADM" id="senhaADM" class="form-control" style="width: 200px" required>
                        <br><button type="submit" class="btn btn-lg" style="background: #222d32; color: #b8c7ce;">Entrar</button><br>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src='js/jquery.min.js' type='text/javascript'></script>
    <script src='js/bootstrap.min.js' type='text/javascript'></script>
    <script src='js/SidebarNav.min.js' type='text/javascript'></script>
    <script>
      $('.sidebar-menu').SidebarNav()
    </script>
  </body>
</html>




<footer>
    <p></p>
</footer>
