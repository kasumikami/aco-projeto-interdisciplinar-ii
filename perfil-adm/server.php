<?php
	include('../connect.php');

	$nome = "";
	$id = 0;

	if (isset($_POST['save'])) {
		$nome = $_POST['nomeCAT'];
		$cadastrar = mysqli_query($connect, "INSERT INTO categoria_obra (nomeCAT) VALUES ('$nome')"); 
		
		if($cadastrar){
			$_SESSION['message'] = "<div class='alert alert-success'>Categoria cadastrada com sucesso!</div>"; 
			header('location: categ-listar.php');
		}else{
			$_SESSION['message'] =  "<div class='alert alert-danger'>Não foi possível cadastrar.</div>";
			header('location: categ-listar.php');
		}
		
    }
	
	if (isset($_POST['edit'])) {
		$id = $_POST['idCAT'];
		$nome = $_POST['nomeCAT'];
		$editar = mysqli_query($connect, "UPDATE categoria_obra SET nomeCAT='$nome' WHERE idCAT=$id");
		
		if($editar){
			$_SESSION['message'] = "<div class='alert alert-success'>Categoria editada com sucesso!</div>"; 
			header('location: categ-listar.php');
		}else{
			$_SESSION['message'] = "<div class='alert alert-danger'>Não foi possível editar.</div>";
			header('location: categ-listar.php');
		}
	}

	if (isset($_GET['del'])) {
		$id = $_GET['del'];
		$deletar = mysqli_query($connect, "DELETE FROM categoria_obra WHERE idCAT=$id");

		if($deletar){
			$_SESSION['message'] = "<div class='alert alert-success'>Categoria deletada com sucesso!</div>"; 
			header('location: categ-listar.php');
		}else{
			$_SESSION['message'] = "<div class='alert alert-danger'>Você não pode deletar esta categoria pois ela é uma chave estrangeira e está sendo usada.</div>";
			header('location: categ-listar.php');
		}
		
	}
?>