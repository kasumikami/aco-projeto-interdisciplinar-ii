<?php include_once '../connect.php';

if (isset($_POST['edit'])) {
    $id = $_POST['idBIB'];
    $nome = $_POST['nomeBIB'];
    $email = $_POST['emailBIB'];
    $telefone = $_POST['telefone'];
    $endereco = $_POST['endereco'];
    $horFun = $_POST['horarioFuncionamento'];
    $login = $_POST['loginBIB'];

    $update = "UPDATE biblioteca
               SET nomeBIB = '$nome',
               emailBIB = '$email',
               telefone = '$telefone',
               endereco = '$endereco',
               horarioFuncionamento = '$horFun',
               loginBIB = '$login'
               WHERE idBIB= $id";
        
    $editar = mysqli_query($connect, $update);
        
    if($editar){
            echo"<script language='javascript' type='text/javascript'>
            alert('Perfil editado com sucesso!');
            window.location.href='listar-bib.php';</script>";
    }
    else{
            echo"<script language='javascript' type='text/javascript'>
            alert('Não foi possível editar.');
            window.location.href='listar-bib.php';</script>";
    }
}

if (isset($_GET['del'])) {
    $id = $_GET['del'];
    $deletar = mysqli_query($connect, "DELETE FROM biblioteca WHERE idBIB=$id");

    if($deletar){
        echo"<script language='javascript' type='text/javascript'>
        alert('Perfil deletado!');
        window.location.href='listar-bib.php';</script>";
    }else{
        echo"<script language='javascript' type='text/javascript'>
        alert('Você não pode deletar este perfil pois há obras literárias vinculadas a ele. Se deseja mesmo deletá-lo, será necessário deletar o acervo primeiro.');
        window.location.href='listar-bib.php';</script>";
    }  
}

?>