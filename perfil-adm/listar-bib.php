<head><title>Listar Bibliotecas - Administrador</title></head>
<?php
    include('head.php');
?>
    <div class='container'>
        <div class='row'>
            <div class='ml-1 mt-5 col-lg-12'>
            <h2>BIBLIOTECAS</h2><br><br>

            <form class="form-inline" method="GET" action="pesquisar-bib.php">
                <input class="form-control form-control-sm mr-3 w-75" type="text" name="pesquisar-bib" placeholder="Pesquisar uma biblioteca" aria-label="Search">
                <i class="fas fa-search" aria-hidden="true"></i>
            </form>
            <?php 
            $res = mysqli_query($connect, "SELECT * FROM biblioteca"); 
            $qtd = $res->num_rows;
            
            if($qtd>0){
                print "<p style='color:#428bca;'>".$qtd." biblioteca(s) encontrada(s). <p>";?>
                <table class="table table-striped table-hover mt-2">
                <tr>
                    <th>ID</th>
                    <th>Biblioteca</th>
                    <th>Email</th>
                    <th>Tel.</th>
                    <th>Endereço</th>
                    <th>Hor. Func.</th>
                    <th>Ação</th>
                </tr>
                <?php while ($row = mysqli_fetch_array($res)) { ?>
                <tr>
                    <td><?php echo $row['idBIB']; ?></td>
                    <td><?php echo $row['nomeBIB']; ?></td>
                    <td><?php echo $row['emailBIB']; ?></td>
                    <td><?php echo $row['telefone']; ?></td>
                    <td><?php echo $row['endereco']; ?></td>
                    <td><?php echo $row['horarioFuncionamento']; ?></td>
                    <td>
                    <a href="editar-bib.php?edit=<?php echo $row['idBIB']; ?>" class="edit_btn"> <i class='fa fa-edit' title='Editar'></i></a><a href="gerenciar-bib.php?del=<?php echo $row['idBIB']; ?>" class="del_btn" onclick="return confirm('Tem certeza que deseja deletar este perfil permanentemente?');"><i class='fa fa-times-circle' title='Excluir' style='color:red;'></i></a>
                    </td>
                <tr>

                <?php } 
                }
                else{
                    print "<div class='alert alert-secondary'>Não há nenhum resultado.</div>";
                }
                ?>
            </div>
        </div>
    </div>