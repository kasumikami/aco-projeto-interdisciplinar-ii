<head><title>Editar Categoria - Administrador</title></head>
<?php include('head.php');

	if (isset($_GET['edit'])) {
		$id = $_GET['edit'];
		$update = true;
		$record = mysqli_query($connect, "SELECT * FROM categoria_obra WHERE idCAT=$id");

		if (count($record) == 1 ) {
			$n = mysqli_fetch_array($record);
			$nome = $n['nomeCAT'];
		}
	}
?>
    <div class='container'>
        <div class='row'>
            <div class='col-lg-9 mt-5'>
                <h2>EDITAR CATEGORIA</h2><br>
                        <form action="server.php" method="post">
                            <input type="hidden" name="idCAT" value="<?php echo $id; ?>">
                            <div><label>Nome da categoria:</label></div>
                                <input type="text" name="nomeCAT" class="form-control" value="<?php echo $nome; ?>">
                            <div><br><button class="btn btn-secondary" type="submit" name="edit">Editar</button>
                            </div>
                        </form>
                </div>
            </div>        
        </div>