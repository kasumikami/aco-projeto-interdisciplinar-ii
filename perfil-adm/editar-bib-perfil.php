<?php 
 include('head.php');

 if (isset($_GET['edit'])) {
    $id = $_GET['edit'];
    $select = mysqli_query($connect, "SELECT * FROM biblioteca WHERE idBIB=$id");

    if (count($select) == 1 ) {
        $n = mysqli_fetch_array($select);
        $nome = $n['nomeBIB'];
        $email = $n['emailBIB'];
        $telefone = $n['telefone'];
        $endereco = $n['endereco'];
        $horFun =  $n['horarioFuncionamento'];
        $login = $n['loginBIB'];
    }
}

?>
<title>Editar Perfil de Biblioteca - Administrador</title> 
</body>
    <div class="container">
        <div class="col-lg-9 ml-5 mt-5">
            <BR><br><h1>EDITAR PERFIL DE BIBLIOTECA</h1><br>
            <?php include_once 'gerenciar-bib.php' ?>
            <form action="gerenciar-bib.php" method="POST" data-toggle="validator">
                <input type="hidden" name="idBIB" value="<?php echo $id; ?>">
                <div class="form-group">
                    <label>Nome da Biblioteca:</label>
                    <input type="text" name="nomeBIB" class="form-control" value="<?php echo $nome; ?>" required autofocus>
                    <label>E-mail:</label>
                    <input type="email" name="emailBIB" class="form-control" value="<?php echo $email; ?>" required>
                    <label>Telefone:</label>
                    <input type="number" name="telefone" class="form-control" value="<?php echo $telefone; ?>" required>
                    <label>Endereço:</label>
                    <input type="text" name="endereco" class="form-control" value="<?php echo $endereco; ?>" required>
                    <label>Horário de funcionamento:</label>
                    <input type="text" name="horarioFuncionamento" class="form-control" value="<?php echo $horFun; ?>" required>
                </div>
                <div class="form-group">
                    <label>ID:</label>
                    <input type="text" name="loginBIB" class="form-control" style="width: 190px" data-minlength="6" data-maxlength="6" value="<?php echo $login; ?>" required>
                </div>
                <div class="form-group">
                    <button class="btn btn-secondary" type="submit" name="edit">Editar</button>
                </div>
            </form>
            </div>
        </div>
    </body>
</html>