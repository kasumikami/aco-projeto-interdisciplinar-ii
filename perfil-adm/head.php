<?php include('server.php');
    if (!isset($_SESSION['loginADM']) and !isset($_SESSION['nivelADM']) && ($_SESSION['nivelADM'] !=1 )){
        header("Location:../adm-login.php?erro=Usuário não logado no sistema");
    }
    if(isset($_SESSION['usuario'])){
        $results = mysqli_query($connect, "SELECT * FROM administrador WHERE idADM=".$_SESSION['usuario']);    
        while ($row = mysqli_fetch_array($results)) { ?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel='stylesheet' href='css/bootstrap.min.css'/>
    <link rel="shortcut icon" href="../img/aco.ico"/>
    <link href="https://fonts.googleapis.com/css2?family=Forum&family=Open+Sans:ital,wght@0,400;1,400;1,800&family=Oswald:wght@300;700&family=PT+Mono&family=Playfair+Display+SC:ital@1&family=Playfair+Display:wght@500&family=Space+Mono&family=Special+Elite&family=Tenali+Ramakrishna&family=Titillium+Web:wght@300&display=swap" rel="stylesheet">
    <script src='js/jquery-3.5.1.slim.min.js'></script>
    <script src='js/popper.min.js'></script>
    <script src='js/bootstrap.min.js'></script>
    <script src="https://kit.fontawesome.com/bf4d0ad315.js" crossorigin="anonymous"></script>
    <title>Perfil - <?php echo strtoupper($row['nomeADM']); ?> (Administrador)</title>
    <style>
        .navbar-nav > li{
        margin-left:30px;
        margin-right:30px;
        }
        .form-inline{width: 700px;}
    </style>
  </head>
  <div class="container mt-5">
    <nav id="navb" class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="container">
            <div id="navbarNavDropdown" class="navbar-collapse collapse">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item active">
                        <a id="item2" class="nav-link" href="listar-bib.php">BIBLIOTECAS <i class="fas fa-book"></i></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        CATEGORIA DE OBRA <i class="fas fa-list"></i> 
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="categ-cadastrar.php">Cadastrar</a>
                            <a class="dropdown-item" href="categ-listar.php">Listar</a>
                        </div>
                    </li>
                    <li class="nav-item active">
                        <a id="item2" class="nav-link" href="../logout.php">SAIR <i class="fas fa-sign-out-alt"></i></a>
                    </li>
                    <li class="nav-item active">
                        <a id="nav-it" class="nav-link text-uppercase" href="perfil.php"><?php echo strtoupper($row['nomeADM']); ?> (ADMINISTRADOR) <i class="fas fa-user-circle"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
<?php }
 }?>
</div>