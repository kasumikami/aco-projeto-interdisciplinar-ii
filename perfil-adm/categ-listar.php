<head><title>Categorias - Administrador</title></head>
<?php
  include('head.php');
?>
    <div class='container'>
        <div class='row'>
          <div class='ml-1 mt-5 col-lg-6'>
                <h2>CATEGORIAS</h2><br>
                
                <!---Mensagem--->
                <?php if (isset($_SESSION['message'])): ?>
                  <div class="msg">
                    <?php 
                      echo $_SESSION['message'];
                      unset($_SESSION['message']);
                    ?>
                  </div>
                <?php endif ?>
                <!-------------->

                <?php $results = mysqli_query($connect, "SELECT * FROM categoria_obra"); 
                $qtd = $results->num_rows;
                if($qtd>0){
                  print "<p style='color:#428bca;'>".$qtd." resultado(s) encontrado(s). <p>";
                ?>
                <table class="table table-striped table-hover">
                    <tr>
                      <th>Categoria</th>
                      <th colspan="2">Ação</th>
                    </tr>
                  
                  <?php while ($row = mysqli_fetch_array($results)) { ?>
                    <tr>
                      <td><?php echo $row['nomeCAT']; ?></td>
                      <td>
                        <a href="categ-editar.php?edit=<?php echo $row['idCAT']; ?>" class="edit_btn"><i class='fa fa-edit' title='Editar'></i></a>
                        <a href="server.php?del=<?php echo $row['idCAT']; ?>" class="del_btn" onclick="return confirm('Tem certeza que deseja deletar esta categoria?');"><i class='fa fa-times-circle' title='Excluir' style='color:red;'></i></a>
                      </td>
                    </tr>
                  <?php } 
                  }
                  else{
                    print "<div class='alert alert-secondary'>Não há nenhum resultado.</div>";
                  }?>
                </table>
                <div>
                  <a class="btn btn-outline-secondary btn-lg btn-block ml-1" href="categ-cadastrar.php" role="button">Cadastrar Nova</a> 
                </div>
                </div>
            </div>        
        </div>
