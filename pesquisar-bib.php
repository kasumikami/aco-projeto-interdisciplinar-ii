<?php include('connect.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta content='IE=edge' http-equiv='X-UA-Compatible'/>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'/>
    <meta content='' name='author'/>
    <meta content='' name='description'/>
    <meta content='' name='keywords'/>
    <link href='css/bootstrap.min.css' media='all' rel='stylesheet' type='text/css'/>
    <link href='css/font-awesome.min.css' media='all' rel='stylesheet' type='text/css'/>
    <link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
    <link rel="shortcut icon" href="img/aco.ico"/>
    <script src="https://kit.fontawesome.com/bf4d0ad315.js" crossorigin="anonymous"></script>
    <title>Bibliotecas - Resultado da Pesquisa</title>
    <style>
    </style>
  </head>
  <body>
    <div class='wrapper'>
      <aside class="sidebar-left navbar navbar-inverse">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home.php" style="height: 75px;">
             <img src="img/aco-logo-white-50.png" alt="logo-book-with-mouse-clicker">
          </a>
        </div>
        <nav class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <li><a href="home.php"><i class="fas fa-home"></i> <span>INICIO</span></a></li>
            <li class="treeview">
              <a href="#">
              <i class="fa fa-edit"></i> <span>Cadastre-se</span>
              <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="cad-bib.php"><i class="fa fa-circle-o"></i> Biblioteca</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fas fa-sign-in-alt"></i> <span>Login</span>
              <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="adm-login.php"><i class="fa fa-circle-o"></i> Administrador</a></li>
                <li><a href="bib-login.php"><i class="fa fa-circle-o"></i> Biblioteca</a></li>
              </ul>
            </li>
            <li><a href="listar-bib.php"><i class="fas fa-book"></i> <span>Bibliotecas</span></a></li>
            <li>
              <form class="form-control-sm" method="GET" action="pesquisar-bib.php">
                  <input class="form-control" type="text" name="pesquisar-bib" placeholder="Pesquisar uma biblioteca" aria-label="Search" style="width: 175px; margin-left: 1em; margin-top: 1em; font-size: 13px;">
              </form>
            </li>
          </ul>
        </nav>
      </aside>
      <div class='container'>
        <div class='row'>
            <div class='ml-1 mt-4 col-lg-12'>
              <br><h1>BIBLIOTECAS - RESULTADO</h1><br>
              <?php 
                $pesquisar = $_GET['pesquisar-bib'];

                    $select = "SELECT * from biblioteca
                    WHERE nomeBIB 
                    LIKE '%$pesquisar%'
                    OR endereco
                    LIKE '%$pesquisar%'";

                    $res = $connect->query($select);
                    @$qtd = $res->num_rows;
                        
                    if($qtd>0){
                        print "<p>".$qtd." resultado(s) para '".$pesquisar."'</p>"; ?>
                        <table class="table table-striped table-hover mt-2">
                        <tr>
                          <th>Biblioteca</th>
                          <th>Endereço</th>
                          <th>Funcionamento</th>
                          <th>Contato</th>
                        </tr>
                        <?php while ($row = mysqli_fetch_array($res)) { ?>
                        <tr>
                          <td><a href="perfil-bib.php?profile=<?php echo $row['idBIB']; ?>">
                            <?php echo $row['nomeBIB']; ?></a>
                          </td>
                          <td><?php echo $row['endereco']; ?></td>
                          <td><?php echo $row['horarioFuncionamento']; ?></td>
                          <td>
                            <p><b>Tel.:</b><?php echo $row['telefone']; ?><p>
                            <p><b>Email:</b><?php echo $row['emailBIB']; ?><p>
                          </td>
                        <tr>

                        <?php } 
                      }
                      else{
                          print "<br><div class='alert alert-info'style='width: 600px' role='alert'>Nenhum resultado encontrado para '".$pesquisar."'.</div>";
                          print "<div><br><br><br><br><br><br><br></div>";
                      }?>
                </div>
            </div>
        </div>
    </div>
    <script src='js/jquery.min.js' type='text/javascript'></script>
    <script src='js/bootstrap.min.js' type='text/javascript'></script>
    <script src='js/SidebarNav.min.js' type='text/javascript'></script>
    <script>
      $('.sidebar-menu').SidebarNav()
    </script>
  </body>
</html>




<footer>
    <p></p>
</footer>