-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 09-Maio-2020 às 00:07
-- Versão do servidor: 5.7.26
-- versão do PHP: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aco`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `administrador`
--

DROP TABLE IF EXISTS `administrador`;
CREATE TABLE IF NOT EXISTS `administrador` (
  `idADM` int(11) NOT NULL AUTO_INCREMENT,
  `nomeADM` varchar(45) NOT NULL,
  `loginADM` varchar(6) NOT NULL,
  `senhaADM` varchar(45) NOT NULL,
  `nivelADM` int(11) NOT NULL,
  PRIMARY KEY (`idADM`),
  UNIQUE KEY `loginADM_UNIQUE` (`loginADM`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--

-- --------------------------------------------------------

--
-- Estrutura da tabela `biblioteca`
--

DROP TABLE IF EXISTS `biblioteca`;
CREATE TABLE IF NOT EXISTS `biblioteca` (
  `idBIB` int(11) NOT NULL AUTO_INCREMENT,
  `nomeBIB` varchar(255) NOT NULL,
  `emailBIB` varchar(100) NOT NULL,
  `telefone` varchar(20) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `horarioFuncionamento` varchar(255) NOT NULL,
  `loginBIB` varchar(6) NOT NULL,
  `senhaBIB` varchar(100) NOT NULL,
  `nivelBIB` int(11) NOT NULL DEFAULT '2',
  PRIMARY KEY (`idBIB`),
  UNIQUE KEY `emailBIB_UNIQUE` (`emailBIB`),
  UNIQUE KEY `telefoneBIB_UNIQUE` (`telefone`),
  UNIQUE KEY `loginBIB_UNIQUE` (`loginBIB`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria_obra`
--

DROP TABLE IF EXISTS `categoria_obra`;
CREATE TABLE IF NOT EXISTS `categoria_obra` (
  `idCAT` int(11) NOT NULL AUTO_INCREMENT,
  `nomeCAT` varchar(45) NOT NULL,
  `administrador_idADM` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCAT`),
  UNIQUE KEY `nomeCAT_UNIQUE` (`nomeCAT`),
  KEY `fk_categoria_obra_administrador1_idx` (`administrador_idADM`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--

-- --------------------------------------------------------

--
-- Estrutura da tabela `obra_literaria`
--

DROP TABLE IF EXISTS `obra_literaria`;
CREATE TABLE IF NOT EXISTS `obra_literaria` (
  `idOBRA` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `autor` varchar(100) NOT NULL,
  `anoPub` year(4) NOT NULL,
  `edicao` int(3) UNSIGNED NOT NULL,
  `editora` varchar(1000) NOT NULL,
  `isbn` varchar(20) NOT NULL,
  `qtCopias` int(10) UNSIGNED NOT NULL,
  `categoria_obra_idCAT` int(11) NOT NULL,
  `biblioteca_idBIB` int(11) NOT NULL,
  PRIMARY KEY (`idOBRA`),
  KEY `fk_obra_literaria_categoria_obra1_idx` (`categoria_obra_idCAT`),
  KEY `fk_obra_literaria_biblioteca1_idx` (`biblioteca_idBIB`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `categoria_obra`
--
ALTER TABLE `categoria_obra`
  ADD CONSTRAINT `fk_categoria_obra_administrador1` FOREIGN KEY (`administrador_idADM`) REFERENCES `administrador` (`idADM`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `obra_literaria`
--
ALTER TABLE `obra_literaria`
  ADD CONSTRAINT `fk_obra_literaria_biblioteca1` FOREIGN KEY (`biblioteca_idBIB`) REFERENCES `biblioteca` (`idBIB`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_obra_literaria_categoria_obra1` FOREIGN KEY (`categoria_obra_idCAT`) REFERENCES `categoria_obra` (`idCAT`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
