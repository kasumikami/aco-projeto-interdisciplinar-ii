<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta content='IE=edge' http-equiv='X-UA-Compatible'/>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'/>
    <meta content='' name='author'/>
    <meta content='O Acervo Online (ACO) é um sistema que reúne bibliotecas e seus acervos individuais para a consulta, 
    cujo principal objetivo é facilitar a  procura do leitor por obras literárias. O acervo online disponibilizará os 
    acervos de todas as bibliotecas cadastradas no sistema para pesquisa pública, assim, quando o leitor estiver à procura 
    de obras, ele pode pesquisar dentro do sistema de forma rápida e eficiente.' name='description'/>
    <meta content='bibliotecas, acervo, aco' name='keywords'/>
    <link href='css/bootstrap.min.css' media='all' rel='stylesheet' type='text/css'/>
    <link href='css/font-awesome.min.css' media='all' rel='stylesheet' type='text/css'/>
    <link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
    <link rel="shortcut icon" href="img/aco.ico"/>
    <link href="https://fonts.googleapis.com/css2?family=Arapey&family=Gentium+Book+Basic&family=Prata&family=Special+Elite&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/bf4d0ad315.js" crossorigin="anonymous"></script>
    <title>ACO - Home</title>
    <style>
      #title1{font-family: 'Special Elite', cursive;}
      .panel-heading{font-family: 'Prata', serif; font-size: 18px;}
      .panel-body{
        text-align: justify; 
        font-family: 'Gentium Book Basic', serif; 
        font-size: 16px;
      }
    </style>
  </head>
  <body>
    <div class='wrapper'>
      <aside class="sidebar-left navbar navbar-inverse">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home.php" style="height: 75px;">
             <img src="img/aco-logo-white-50.png" alt="logo-book-with-mouse-clicker">
          </a>
        </div>
        <nav class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <li><a href="home.php"><i class="fas fa-home"></i> <span>INICIO</span></a></li>
            <li class="treeview">
              <a href="#">
              <i class="fa fa-edit"></i> <span>Cadastre-se</span>
              <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="cad-bib.php"><i class="fa fa-circle-o"></i> Biblioteca</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fas fa-sign-in-alt"></i> <span>Login</span>
              <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="adm-login.php"><i class="fa fa-circle-o"></i> Administrador</a></li>
                <li><a href="bib-login.php"><i class="fa fa-circle-o"></i> Biblioteca</a></li>
              </ul>
            </li>
            <li><a href="listar-bib.php"><i class="fas fa-book"></i> <span>Bibliotecas</span></a></li>
            <li>
                <form class="form-control-sm" method="GET" action="pesquisar.php">
                    <input class="form-control" type="text" name="pesquisar" placeholder="Pesquisar obra ou autor" aria-label="Search" style="width: 175px; margin-left: 1em; margin-top: 1em; font-size: 13px;">
                </form>
            </li>
          </ul>
        </nav>
      </aside>
      <div class='content container-fluid'>
        <h1 id="title1"> ACO<small> ACERVO ONLINE</small></h1><br>
        <div class="panel panel-default">
          <div class="panel-heading">Sobre o Acervo</div>
          <div class="panel-body">
            <p>O Acervo Online é um sistema que reúne bibliotecas e seus acervos individuais para a consulta, cujo
             principal objetivo é facilitar a  procura do leitor por obras literárias. O ACO disponibilizará os acervos
             de todas as bibliotecas cadastradas no sistema para pesquisa pública, assim, quando o leitor estiver à
             procura de obras, ele pode pesquisar dentro do sistema de forma rápida e eficiente. A reunião  dos acervos
             possibilitará  ao leitor  saber se o livro desejado está em uma  biblioteca sem deslocar-se ao local 
             físico. Ele permite que os usuários acessem um só site  ao invés de consultar todos os site das bibliotecas
             individualmente, pois basta entrar no sistema e procurar por um título de uma obra ou um autor e ele indicará
             em quais bibliotecas poderá encontrá-lo.
            </p>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="panel panel-default">
              <div class="panel-heading">Content</div>
              <div class="panel-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pellentesque sit amet urna consectetur aliquam. Integer at ullamcorper dui. In quis orci et risus ultricies elementum in non tortor. Proin sapien mi, tempor vel condimentum porttitor, sagittis efficitur augue. Mauris auctor erat in orci ullamcorper, ac rhoncus risus accumsan. Aliquam luctus elementum elit id pharetra. Pellentesque posuere, tellus eu vehicula placerat, elit neque auctor nulla, in sagittis velit nulla vitae sapien. Suspendisse tempus tincidunt quam eget eleifend. Aenean vel iaculis nulla.
                  Vestibulum vitae sapien feugiat, laoreet erat tempus, tempus tellus. Cras ac sapien lectus. Proin tristique sed est quis scelerisque. Nunc efficitur cursus massa, vel volutpat tortor aliquet nec. Sed pharetra lacus vel ex efficitur sollicitudin. Duis id est in lectus vehicula ornare scelerisque id mauris. Phasellus convallis dapibus sem, sit amet sagittis tortor sodales in. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc et feugiat sapien, ut congue enim. Etiam tempus turpis ac sem iaculis, at porttitor orci euismod.
                  Nulla semper ante eu tortor faucibus dapibus. Pellentesque id massa tincidunt, sodales tellus sit amet, pulvinar mauris. Sed hendrerit in ipsum pulvinar sagittis. Pellentesque auctor ut est vel elementum. Sed ullamcorper, velit at laoreet convallis, arcu felis tristique neque, eu placerat purus nunc in magna. Praesent sit amet augue elementum, mattis orci id, ultricies diam. Maecenas quis fringilla dolor. Donec pharetra cursus nunc. Aliquam scelerisque volutpat consectetur.
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="panel panel-default">
              <div class="panel-heading">Content</div>
              <div class="panel-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pellentesque sit amet urna consectetur aliquam. Integer at ullamcorper dui. In quis orci et risus ultricies elementum in non tortor. Proin sapien mi, tempor vel condimentum porttitor, sagittis efficitur augue. Mauris auctor erat in orci ullamcorper, ac rhoncus risus accumsan. Aliquam luctus elementum elit id pharetra. Pellentesque posuere, tellus eu vehicula placerat, elit neque auctor nulla, in sagittis velit nulla vitae sapien. Suspendisse tempus tincidunt quam eget eleifend. Aenean vel iaculis nulla.
                  Vestibulum vitae sapien feugiat, laoreet erat tempus, tempus tellus. Cras ac sapien lectus. Proin tristique sed est quis scelerisque. Nunc efficitur cursus massa, vel volutpat tortor aliquet nec. Sed pharetra lacus vel ex efficitur sollicitudin. Duis id est in lectus vehicula ornare scelerisque id mauris. Phasellus convallis dapibus sem, sit amet sagittis tortor sodales in. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc et feugiat sapien, ut congue enim. Etiam tempus turpis ac sem iaculis, at porttitor orci euismod.
                  Nulla semper ante eu tortor faucibus dapibus. Pellentesque id massa tincidunt, sodales tellus sit amet, pulvinar mauris. Sed hendrerit in ipsum pulvinar sagittis. Pellentesque auctor ut est vel elementum. Sed ullamcorper, velit at laoreet convallis, arcu felis tristique neque, eu placerat purus nunc in magna. Praesent sit amet augue elementum, mattis orci id, ultricies diam. Maecenas quis fringilla dolor. Donec pharetra cursus nunc. Aliquam scelerisque volutpat consectetur.
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">Content</div>
          <div class="panel-body">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pellentesque sit amet urna consectetur aliquam. Integer at ullamcorper dui. In quis orci et risus ultricies elementum in non tortor. Proin sapien mi, tempor vel condimentum porttitor, sagittis efficitur augue. Mauris auctor erat in orci ullamcorper, ac rhoncus risus accumsan. Aliquam luctus elementum elit id pharetra. Pellentesque posuere, tellus eu vehicula placerat, elit neque auctor nulla, in sagittis velit nulla vitae sapien. Suspendisse tempus tincidunt quam eget eleifend. Aenean vel iaculis nulla.
              Vestibulum vitae sapien feugiat, laoreet erat tempus, tempus tellus. Cras ac sapien lectus. Proin tristique sed est quis scelerisque. Nunc efficitur cursus massa, vel volutpat tortor aliquet nec. Sed pharetra lacus vel ex efficitur sollicitudin. Duis id est in lectus vehicula ornare scelerisque id mauris. Phasellus convallis dapibus sem, sit amet sagittis tortor sodales in. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc et feugiat sapien, ut congue enim. Etiam tempus turpis ac sem iaculis, at porttitor orci euismod.
              Nulla semper ante eu tortor faucibus dapibus. Pellentesque id massa tincidunt, sodales tellus sit amet, pulvinar mauris. Sed hendrerit in ipsum pulvinar sagittis. Pellentesque auctor ut est vel elementum. Sed ullamcorper, velit at laoreet convallis, arcu felis tristique neque, eu placerat purus nunc in magna. Praesent sit amet augue elementum, mattis orci id, ultricies diam. Maecenas quis fringilla dolor. Donec pharetra cursus nunc. Aliquam scelerisque volutpat consectetur.
            </p>
          </div>
        </div>
      </div>
    </div>
    <script src='js/jquery.min.js' type='text/javascript'></script>
    <script src='js/bootstrap.min.js' type='text/javascript'></script>
    <script src='js/SidebarNav.min.js' type='text/javascript'></script>
    <script>
      $('.sidebar-menu').SidebarNav()
    </script>
  </body>
</html>




<footer>
    <p></p>
</footer>
