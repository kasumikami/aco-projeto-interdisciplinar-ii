<?php include('head.php');
    
    if(isset($_GET['upd_pass'])) {
    $id = $_GET['upd_pass']; 
    }
?>
<div class="container mt-5">
        <div class="col-lg-6 ml-5">
            <h1>Alterar Senha</h1><br>
                <form method="POST" action="gerenciar-bib.php">
                    <input type="hidden" name="idBIB" value="<?php echo $id; ?>">
                    <label>Digite sua senha atual:</label>
                    <input type="password" name="senha" class="form-control" data-minlength="6" data-maxlength="6" required autofocus>
                    <label>Digite sua nova senha:</label>
                    <input type="password" name="nova_senha" class="form-control" data-minlength="6" data-maxlength="6" required>
                    <span class="help-block">Confirme a nova senha:</span>
                    <input type="password" name="conf_nova_senha" class="form-control" data-minlength="6" data-maxlength="6" required>
                    <div class="form-group">
                        <br><button class="btn btn-secondary" type="submit" name="upd_pass">Alterar</button>
                    </div>
                </form>
            </div>
        </div>