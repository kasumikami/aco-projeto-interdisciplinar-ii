<?php
    include('head.php');
        $results = mysqli_query($connect, "SELECT * FROM biblioteca WHERE idBIB=".$_SESSION['usuario']);    
        while ($row = mysqli_fetch_array($results)) { ?>
        <title>Perfil - <?php echo $row['nomeBIB']; ?></title> 
    <body>
        <div class="container">
            <div class='col-lg-12 ml-5 mt-4'>
                <h1>PERFIL</h1>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <hr><p><b>USUÁRIO <?php echo strtoupper($row['loginBIB']); ?>  </b><a href="editar.php" class="edit_btn"><i class='fas fa-edit' title='Editar' style='color:#0275d8;'></i></a>
                            <a href="gerenciar-bib.php?del=<?php echo $row['idBIB']; ?>" class="del_btn" onclick="return confirm('Tem certeza que deseja deletar seu perfil permanentemente?');"><i class='fa fa-times-circle' title='Excluir' style='color:red;'></i></a><p><hr>
                        </div>
                        <div class="panel-body">
                            <b><p>Nome: </b><?php echo $row['nomeBIB']; ?><p><hr>
                            <b><p>Email: </b><?php echo $row['emailBIB']; ?><p><hr>
                            <b><p>Telefone: </b><?php echo $row['telefone']; ?><p><hr>
                            <b><p>Endereço: </b><?php echo $row['endereco']; ?><p><hr>
                            <b><p>Horário de Funcionamento: </b><?php echo $row['horarioFuncionamento']; ?><p><hr>
                        </div>
                    </div>
            </div>
        </div>
        <?php } ?>
    </body>