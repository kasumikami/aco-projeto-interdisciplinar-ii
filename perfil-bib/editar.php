<?php 
    include('head.php');
    $results = mysqli_query($connect, "SELECT * FROM biblioteca WHERE idBIB=".$_SESSION['usuario']);    
    while ($row = mysqli_fetch_array($results)) { ?>
    
    <title>Editar - <?php echo $row['nomeBIB']; ?></title> 
    <div class="container">
        <div class="col-lg-12 ml-4 mt-4 mr-3">
            <b><p><i class="far fa-edit"></i> EDITAR<p></b>
                <a href="editar-perfil.php?edit=<?php echo $row['idBIB']; ?>">
                    <div class="card">
                        <div class="card-body">
                            Editar Perfil <i class="fas fa-chevron-right"></i>
                        </div>
                    </div>
                </a><br>
                <a href="alt-senha.php?upd_pass=<?php echo $row['idBIB']; ?>">
                    <div class="card">
                        <div class="card-body">
                            Alterar Senha <i class="fas fa-chevron-right"></i>
                        </div>
                    </div>
                </a>
            </div>
    </div>
<?php } ?>