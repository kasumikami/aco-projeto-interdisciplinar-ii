<?php 
 include('head.php');
?>
    <title>Cadastrar Uma Obra - Biblioteca</title> 
    <body>     
        <div class="container">
            <div class="col-lg-6 ml-5 mt-4">
                <h1>CADASTRAR UMA OBRA</h1><br>
                <form action="server.php" method="post">
                    <?php 
                        $query = "SELECT * FROM categoria_obra";
                        $result = mysqli_query($connect, $query);
                        echo "<label>Categoria: </label>";
                        echo "<select class='form-control' name='categoria_obra_idCAT'>";
                        echo "<option value =''>SELECIONE A CATEGORIA</option>";
                            
                        while ($row = $result->fetch_object()){
                            $nome_cat = $row->nomeCAT;
                            echo "<option value='".$row->idCAT."'>".$nome_cat."</option>";
                        }
                        echo "</select>";
                    ?>
                    <label>Título: </label>
                    <input type="text" name="titulo" class="form-control" value=""required>
                    <label>Autor: </label>
                    <input type="text" name="autor" class="form-control" value=""required>
                    <label>Ano de publicação: </label>
                    <input type="number" name="anoPub" class="form-control" value=""required>
                    <label>Edição: </label>
                    <input type="number" name="edicao" class="form-control" value=""required>
                    <label>Editora: </label>
                    <input type="text" name="editora" class="form-control" value=""required>
                    <label>ISBN: </label>
                    <input type="text" name="isbn" class="form-control" value=""required>
                    <label>Qtd. cópias disponíveis: </label>
                    <input type="number" name="qtCopias" class="form-control" value=""required>
                    <div class="form-group">
                        <br><button class="btn btn-outline-secondary" type="submit" name="save">Cadastrar</button>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>