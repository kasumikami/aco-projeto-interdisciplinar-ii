<?php include_once 'gerenciar-bib.php';
  
    if (!isset($_SESSION['loginBIB']) and !isset($_SESSION['nivelBIB']) && ($_SESSION['nivelBIB'] !=2 )){
        header("Location:../bib-login.php?erro=Usuário não logado no sistema");
    }

    if(isset($_SESSION['usuario'])){
        $results = mysqli_query($connect, "SELECT * FROM biblioteca WHERE idBIB=".$_SESSION['usuario']);     
        while ($row = mysqli_fetch_array($results)) { ?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://kit.fontawesome.com/bf4d0ad315.js" crossorigin="anonymous"></script>
    <link rel="shortcut icon" href="../img/aco.ico"/>
    <link href="https://fonts.googleapis.com/css2?family=Forum&family=Open+Sans:ital,wght@0,400;1,400;1,800&family=Oswald:wght@300;700&family=PT+Mono&family=Playfair+Display+SC:ital@1&family=Playfair+Display:wght@500&family=Space+Mono&family=Special+Elite&family=Tenali+Ramakrishna&family=Titillium+Web:wght@300&display=swap" rel="stylesheet">
    <link rel='stylesheet' href='css/bootstrap.min.css'/>
    <script src='js/jquery-3.5.1.slim.min.js'></script>
    <script src='js/popper.min.js'></script>
    <script src='js/bootstrap.min.js'></script>
    <style>

        .navbar-nav > li{
        margin-left:30px;
        margin-right:30px;
        }
        .panel-heading {
        background:#D9D9D9;}
        
        p{margin-left: 12px;}

        a{color: #6c757d;}

        body{padding-top: 70px;}
        
        .form-inline {width: 1050px;}
    </style>
  </head>
  <body>
    <nav id="navb" class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
            <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
                <div class="navbar-collapse collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item active">
                                <a id="nav-it" class="nav-link text-uppercase" href="perfil.php"><?php echo $row['nomeBIB']; ?> <i class="fas fa-user"></i></a>
                            </li>
                        <li class="nav-item active">
                            <a id="item2" class="nav-link" href="cad-obra.php">CADASTRAR UMA OBRA <i class="fas fa-feather"></i></a>
                        </li>
                        <li class="nav-item active">
                            <a id="item2" class="nav-link" href="meu-acervo.php">MEU ACERVO <i class="fas fa-book"></i></a>
                        </li>
                        <li class="nav-item active">
                            <a id="item2" class="nav-link" href="../logout.php">SAIR <i class="fas fa-sign-out-alt"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    <?php } 
}?>

