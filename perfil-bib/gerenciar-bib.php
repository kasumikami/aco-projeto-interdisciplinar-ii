<?php
        include_once 'connect.php';

        if(isset($_POST['save'])){
            $nome = $_POST['nomeBIB'];
            $email = $_POST['emailBIB'];
            $telefone = $_POST['telefone'];
            $endereco = $_POST['endereco'];
            $horFun = $_POST['horarioFuncionamento'];
            $login = $_POST['loginBIB'];
            $senha1 =  $_POST['senha1'];
            $senha2 =  $_POST['senha2'];

            $select1 = "SELECT * FROM biblioteca WHERE emailBIB = '$email'";

            $select2 = "SELECT * FROM biblioteca WHERE loginBIB = '$login'";
            $verif1 = mysqli_query($connect, $select1);
            $verif2 = mysqli_query($connect, $select2);
            if(mysqli_num_rows($verif1) > 0){
                echo"<script language='javascript' type='text/javascript'>
                alert('O email informado já está cadastrado.');
                window.location.href='../cad-bib.php';</script>";
                die();
            }
            elseif (mysqli_num_rows($verif2)) {
                echo"<script language='javascript' type='text/javascript'>
                alert('O usuário informado já existe.');
                window.location.href='../cad-bib.php';</script>";
                die();
            }
            elseif($senha1 != $senha2){
                echo"<script language='javascript' type='text/javascript'>
                alert('As senhas não conferem.');
                window.location.href='../cad-bib.php';</script>";
                die();
            }
            else{

                $senha = SHA1($senha1);
                $insert = "INSERT INTO biblioteca (nomeBIB, emailBIB, telefone, endereco, horarioFuncionamento, loginBIB, senhaBIB)
                           VALUES ('$nome', '$email', '$telefone', '$endereco', '$horFun', '$login', '$senha')";
                $salvar = mysqli_query($connect, $insert);

                if($salvar){
                    echo"<script language='javascript' type='text/javascript'>
                    alert('Usuário cadastrado com sucesso!');window.location.
                    href='perfil.php'</script>";
                }
                else{
                    echo"<script language='javascript' type='text/javascript'>
                    alert('Não foi possível cadastrar.');window.location
                    .href='cad.bib.php'</script>";
                }
            }
        }

        if (isset($_POST['edit'])) {
            $id = $_POST['idBIB'];
            $nome = $_POST['nomeBIB'];
            $email = $_POST['emailBIB'];
            $telefone = $_POST['telefone'];
            $endereco = $_POST['endereco'];
            $horFun = $_POST['horarioFuncionamento'];
            $login = $_POST['loginBIB'];

            $update = "UPDATE biblioteca
                       SET nomeBIB = '$nome',
                       emailBIB = '$email',
                       telefone = '$telefone',
                       endereco = '$endereco',
                       horarioFuncionamento = '$horFun',
                       loginBIB = '$login'
                       WHERE idBIB= $id";
                
            $editar = mysqli_query($connect, $update);
                
            if($editar){
                    echo"<script language='javascript' type='text/javascript'>
                    alert('Perfil editado com sucesso!');
                    window.location.href='perfil.php';</script>";
            }
            else{
                    echo"<script language='javascript' type='text/javascript'>
                    alert('Não foi possível editar.');
                    window.location.href='editar-perfil.php';</script>";
            }
        }

        if(isset($_POST['upd_pass'])){
            $id = $_POST['idBIB'];
            $senha = SHA1($_POST['senha']);
            $nova_senha = $_POST['nova_senha'];
            $conf_nova_senha = $_POST['conf_nova_senha'];

            $select = "SELECT senhaBIB FROM biblioteca WHERE idBIB = $id";
            $res = mysqli_query($connect, $select);
            while ($row = mysqli_fetch_array($res)) {
                    $get_senha = $row['senhaBIB'];

                    if($senha != $get_senha){
                        echo"<script language='javascript' type='text/javascript'>
                        alert('Você informou uma senha atual incorreta.');
                        window.location.href='editar.php';</script>";
                        die();
                    }
                    elseif($nova_senha != $conf_nova_senha){
                        echo"<script language='javascript' type='text/javascript'>
                        alert('As novas senhas não conferem.');
                        window.location.href='editar.php';</script>";
                        die();
                    }
                    else{
        
                        $upd_senha = SHA1($nova_senha);
                        $update = "UPDATE biblioteca
                                   SET senhaBIB = '$upd_senha'
                                   WHERE idBIB = $id";
                        $alterar = mysqli_query($connect, $update);
                        if($alterar){
                            echo"<script language='javascript' type='text/javascript'>
                            alert('Senha alterada com sucesso! Faça login novamente.');
                            window.location.href='../logout.php';</script>";
                        }
                        else{
                            echo"<script language='javascript' type='text/javascript'>
                            alert('Não foi possível alterar sua senha.');
                            window.location.href='#';</script>";
                        }
                    }
            }    
        }

        if (isset($_GET['del'])) {
            $id = $_GET['del'];
            $deletar = mysqli_query($connect, "DELETE FROM biblioteca WHERE idBIB=$id");

            if($deletar){
                echo"<script language='javascript' type='text/javascript'>
                alert('Sua conta foi deletada.');
                window.location.href='../logout.php';</script>";
            }else{
                echo"<script language='javascript' type='text/javascript'>
                alert('Você não pode deletar sua conta pois há obras literárias vinculadas a ela. Se deseja mesmo deletá-la, será necessário deletar seu acervo primeiro.');
                window.location.href='perfil.php';</script>";
            }  
        }

?>