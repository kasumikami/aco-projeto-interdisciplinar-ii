<?php 
 include('head.php');
?>
<title>Meu Acervo - Biblioteca</title>
<body>
    <div class="container">
        <div class="col-lg-12 ml-5">
            <br><h1>MEU ACERVO</h1>

            <!---Mensagem--->
            <?php if (isset($_SESSION['message'])): ?>
                <div class="msg">
                    <?php 
                    echo $_SESSION['message'];
                    unset($_SESSION['message']);
                    ?>
                </div>
            <?php endif ?>       
            <!-------------->
        
            <?php
                $query = "SELECT  ob.*, bb.* 
                FROM obra_literaria AS ob
                INNER JOIN biblioteca AS bb
                ON ob.biblioteca_idBIB = bb.idBIB
                WHERE bb.idBIB 
                LIKE ".$_SESSION['usuario'];

                $res = $connect->query($query);
                $qtd = $res->num_rows;

                
                if($qtd>0){ 
                    echo "<i class='fas fa-book fa-sm'></i> ".$qtd." Obra(s)<br><br>"; ?>
                    <form class="form-inline" method="GET" action="pesquisar-obra.php">
                        <input class="form-control form-control-sm mr-3 w-75" type="text" name="pesquisar-obra" placeholder="Pesquisar uma obra ou autor" aria-label="Search">
                        <i class="fas fa-search" aria-hidden="true"></i>
                    </form>

                   <?php  while($row = $res->fetch_object()) { ?>
                    <br>
                        <div class="row">
                            <div class="col-md-9">
                                <div class="card">
                                    <h5 class="card-header" style="background: #d7d9da"></b><?php echo $row->titulo ?>
                                    <a href="editar-obra.php?edit=<?php echo $row->idOBRA; ?>" class="edit_btn" style='color: #0275d8;'><i class='fa fa-edit fa-xs' title='Editar'></i></a>
                                    <a href="server.php?del=<?php echo $row->idOBRA ?>" class="del_btn" onclick="return confirm('Tem certeza que deseja deletar esta obra?');"><i class='fa fa-times-circle fa-xs' title='Excluir' style='color:red;'></i></a></h5>
                                        <div class="card-body">
                                        <p class="card-text"><b>Autor(es): </b><?php echo $row->autor ?></p>
                                        <p class="card-text"><b>Ano de Publicação: </b><?php echo $row->anoPub ?></p>
                                        <p class="card-text"><b>Edição: </b><?php echo $row->edicao ?></p>
                                        <p class="card-text"><b>Editora: </b><?php echo $row->editora ?></p>
                                        <p class="card-text"><b>ISBN: </b><?php echo $row->isbn ?></p>
                                        <p class="card-text"><b>Qtd. Cópias: </b><?php echo $row->qtCopias?></p>
                                        
                                        <?php 
                                        //categoria
                                        $idcat = $row->categoria_obra_idCAT;
                                        $categ = "SELECT nomeCAT
                                                    FROM categoria_obra
                                                    WHERE idCAT
                                                    LIKE $idcat";

                                        $result = mysqli_query($connect, $categ);
                                        $row = mysqli_fetch_assoc($result);?>
                                        <p class="card-text"><b>Categoria: </b><?php echo $row["nomeCAT"]?></p>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    <br>
                <?php } 
            }else{
                print "<div class='alert alert-secondary'>Não há obras cadastradas.</div>";
            }
            
            ?>
        </div>
    </div>
</body>
</html>
