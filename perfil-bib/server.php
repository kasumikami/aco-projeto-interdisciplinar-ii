<?php 
        include('../connect.php');

        if(isset($_POST['save'])){
            $categoria = $_POST['categoria_obra_idCAT'];
            $titulo = $_POST['titulo'];
            $autor = $_POST['autor'];
            $anoPub = $_POST['anoPub'];
            $edicao = $_POST['edicao'];
            $editora =  $_POST['editora'];
            $isbn = $_POST['isbn'];
            $qtCopias = $_POST['qtCopias'];


            $get_bib = mysqli_query($connect, "SELECT * FROM biblioteca WHERE idBIB=".$_SESSION['usuario']);
            $row1 = mysqli_fetch_array($get_bib);
            $bib = $row1['idBIB'];

            $insert = ("INSERT INTO obra_literaria (titulo, autor, anoPub, edicao, editora, isbn, qtCopias, categoria_obra_idCAT, biblioteca_idBIB)
                        VALUES ('$titulo', '$autor', '$anoPub', '$edicao', '$editora', '$isbn', '$qtCopias', '$categoria', '$bib')");
            
            $cadastrar = mysqli_query($connect, $insert);

            if($cadastrar){
                $_SESSION['message'] = "<div class='alert alert-success'>Obra cadastrada com sucesso!</div>"; 
			    header('location: meu-acervo.php');
            }else{
                $_SESSION['message'] =  "<div class='alert alert-danger'>Não foi possível cadastrar.</div>";
			    header('location: meu-acervo.php');
            }  
        }

        if (isset($_POST['edit'])) {
            $id = $_POST['idOBRA'];
            $categoria = $_POST['categoria_obra_idCAT'];
            $titulo = $_POST['titulo'];
            $autor = $_POST['autor'];
            $anoPub = $_POST['anoPub'];
            $edicao = $_POST['edicao'];
            $editora =  $_POST['editora'];
            $isbn = $_POST['isbn'];
            $qtCopias = $_POST['qtCopias'];
            
            $update = "UPDATE obra_literaria
                       SET titulo = '$titulo',
                       autor = '$autor',
                       anoPub = '$anoPub',
                       edicao = '$edicao',
                       editora = '$editora',
                       isbn = '$isbn',
                       qtCopias = '$qtCopias',
                       categoria_obra_idCAT = '$categoria'
                       WHERE idOBRA = $id";

            $editar = mysqli_query($connect, $update);
            
            if($editar){
                $_SESSION['message'] = "<div class='alert alert-success'>Obra editada com sucesso!</div>"; 
                header('location: meu-acervo.php');
            }else{
                $_SESSION['message'] = "<div class='alert alert-danger'>Não foi possível editar.</div>";
                header('location: meu-acervo.php');
            }
        }
        
        if (isset($_GET['del'])) {
            $id = $_GET['del'];
            $deletar = mysqli_query($connect, "DELETE FROM obra_literaria WHERE idOBRA=$id");
        
            if($deletar){
                $_SESSION['message'] = "<div class='alert alert-success'>Obra deletada com sucesso!</div>"; 
                header('location: meu-acervo.php');
            }else{
                $_SESSION['message'] = "<div class='alert alert-danger'>Não foi possível deletar.</div>";
                header('location: meu-acervo.php');
            }
        }

?>