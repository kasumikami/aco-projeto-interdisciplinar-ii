<?php 
 include('head.php');

	if (isset($_GET['edit'])) {
		$id = $_GET['edit'];
		$select = mysqli_query($connect, "SELECT * FROM obra_literaria WHERE idOBRA=$id");

		if (count($select) == 1 ) {
            $n = mysqli_fetch_array($select);
			$titulo = $n['titulo'];
            $autor = $n['autor'];
            $anoPub = $n['anoPub'];
            $edicao = $n['edicao'];
            $editora =  $n['editora'];
            $isbn = $n['isbn'];
            $qtCopias = $n['qtCopias'];

		}
	}
?>
<title>Editar Obra - Biblioteca</title> 
</body>
    <div class="container mt-5">
        <div class="col-lg-6">
            <h1>Editar Obra</h1><br>
            <form action="server.php" method="post">
                <input type="hidden" name="idOBRA" value="<?php echo $id; ?>">
                    <?php 
                        $query = "SELECT * FROM categoria_obra";
                        $result = mysqli_query($connect, $query);
                        echo "<label>Categoria: </label>";
                        echo "<select class='form-control' name='categoria_obra_idCAT'>";
                        echo "<option value =''>SELECIONE A CATEGORIA</option>";
                        
                        while ($row = $result->fetch_object()){
                            $nome_cat = $row->nomeCAT;
                            echo "<option value='".$row->idCAT."'>".$nome_cat."</option>";
                        }
                        echo "</select>";
                    ?>
                    <label>Título: </label>
                    <input type="text" name="titulo" class="form-control" value="<?php echo $titulo; ?>"required>
                    <label>Autor: </label>
                    <input type="text" name="autor" class="form-control" value="<?php echo $autor; ?>"required>
                    <label>Ano de publicação: </label>
                    <input type="number" name="anoPub" class="form-control" value="<?php echo $anoPub; ?>"required>
                    <label>Edição: </label>
                    <input type="number" name="edicao" class="form-control" value="<?php echo $edicao; ?>"required>
                    <label>Editora: </label>
                    <input type="text" name="editora" class="form-control" value="<?php echo $editora; ?>"required>
                    <label>ISBN: </label>
                    <input type="text" name="isbn" class="form-control" value="<?php echo $isbn; ?>"required>
                    <label>Qtd. cópias disponíveis: </label>
                    <input type="number" name="qtCopias" class="form-control" value="<?php echo $qtCopias; ?>"required>
                    <div class="form-group">
                        <br><button class="btn btn-secondary" type="submit" name="edit">Editar</button>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
