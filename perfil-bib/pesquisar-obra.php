<?php
include('head.php');
?>
<title>Pesquisar Meu Acervo - Biblioteca</title>
<body>
<div class="container">
    <div class="col-lg-12 ml-5">
        <br><h1>MEU ACERVO - RESULTADOS</h1><br><br>
            <!---Mensagem--->
                <?php if (isset($_SESSION['message'])): ?>
                <div class="msg">
                    <?php 
                    echo $_SESSION['message'];
                    unset($_SESSION['message']);
                    ?>
                </div>
            <?php endif ?>       
            <!-------------->
        <form class="form-inline" method="GET" action="pesquisar-obra.php">
            <input class="form-control form-control-sm mr-3 w-75" type="text" name="pesquisar-obra" placeholder="Pesquisar uma obra ou autor" aria-label="Search">
            <i class="fas fa-search" aria-hidden="true"></i>
        </form>
        <?php 

            $bib = $_SESSION['usuario'];
            $pesquisar = $_GET['pesquisar-obra'];

            $query = "SELECT ob.*, bb.* 
                      FROM obra_literaria AS ob
                      INNER JOIN biblioteca AS bb
                      ON ob.biblioteca_idBIB = bb.idBIB
                      WHERE bb.idBIB 
                      LIKE $bib
                      AND (ob.titulo LIKE '%$pesquisar%' OR ob.autor LIKE '%$pesquisar%')";

              $res = $connect->query($query);
              @$qtd = $res->num_rows;

              if($qtd>0){
                print "<br><br><p style='color: #0275d8;'>".$qtd." resultado(s) para '".$pesquisar."'</p>";
                while($row = $res->fetch_object()) { ?>   
                    <div>
                        <div class="col-md-9">
                            <div class="card">
                                <h5 class="card-header" style="background: #d7d9da"></b><?php echo $row->titulo ?>
                                <a href="editar-obra.php?edit=<?php echo $row->idOBRA; ?>" class="edit_btn" style='color: #0275d8;'><i class='fa fa-edit fa-xs' title='Editar'></i></a>
                                <a href="server.php?del=<?php echo $row->idOBRA ?>" class="del_btn" onclick="return confirm('Tem certeza que deseja deletar esta obra?');"><i class='fa fa-times-circle fa-xs' title='Excluir' style='color:red;'></i></a></h5>
                                    <div class="card-body">
                                    <p class="card-text"><b>Autor(es): </b><?php echo $row->autor ?></p>
                                    <p class="card-text"><b>Ano de Publicação: </b><?php echo $row->anoPub ?></p>
                                    <p class="card-text"><b>Edição: </b><?php echo $row->edicao ?></p>
                                    <p class="card-text"><b>Editora: </b><?php echo $row->editora ?></p>
                                    <p class="card-text"><b>ISBN: </b><?php echo $row->isbn ?></p>
                                    <p class="card-text"><b>Qtd. Cópias: </b><?php echo $row->qtCopias?></p>
                                    
                                    <?php 
                                    //categoria
                                    $idcat = $row->categoria_obra_idCAT;
                                    $nome_cat = "SELECT nomeCAT
                                                 FROM categoria_obra
                                                 WHERE idCAT
                                                 LIKE $idcat";

                                    $result = mysqli_query($connect, $nome_cat);
                                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);?>
                                    <p class="card-text"><b>Categoria: </b><?php echo $row["nomeCAT"]?></p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                <br>
            <?php } 
        }
        else{
            print "<br><div class='alert alert-info'style='width: 600px' role='alert'>Nenhum resultado encontrado para '".$pesquisar."'.</div>";
        }?>
    </div>
</div>
</body>
</html>