<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta content='IE=edge' http-equiv='X-UA-Compatible'/>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'/>
    <meta content='' name='author'/>
    <meta content='' name='description'/>
    <meta content='' name='keywords'/>
    <link href='css/bootstrap.min.css' media='all' rel='stylesheet' type='text/css'/>
    <link href='css/font-awesome.min.css' media='all' rel='stylesheet' type='text/css'/>
    <link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
    <link rel="shortcut icon" href="img/aco.ico"/>
    <script src="https://kit.fontawesome.com/bf4d0ad315.js" crossorigin="anonymous"></script>
    <title>ACO - Resultado da Pesquisa</title>
  </head>
  <body>
    <div class='wrapper'>
      <aside class="sidebar-left navbar navbar-inverse">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home.php" style="height: 75px;">
             <img src="img/aco-logo-white-50.png" alt="logo-book-with-mouse-clicker">
          </a>
        </div>
        <nav class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <li><a href="home.php"><i class="fas fa-home"></i> <span>INICIO</span></a></li>
            <li class="treeview">
              <a href="#">
              <i class="fa fa-edit"></i> <span>Cadastre-se</span>
              <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="bib-cadastrar.php"><i class="fa fa-circle-o"></i> Biblioteca</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fas fa-sign-in-alt"></i> <span>Login</span>
              <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="adm-login.php"><i class="fa fa-circle-o"></i> Administrador</a></li>
                <li><a href="bib-login.php"><i class="fa fa-circle-o"></i> Biblioteca</a></li>
              </ul>
            </li>
            <li><a href="listar-bib.php"><i class="fas fa-book"></i> <span>Bibliotecas</span></a></li>
            <li>
                <form class="form-control-sm" method="GET" action="pesquisar.php">
                    <input class="form-control" type="text" name="pesquisar" placeholder="Pesquisar obra ou autor" aria-label="Search" style="width: 175px; margin-left: 1em; margin-top: 1em; font-size: 13px;">
                </form>
            </li>
          </ul>
        </nav>
      </aside>
      <div class="container">
        <div class="row">
          <div class="col-lg-9">
            <br><h2>PESQUISA - RESULTADO</h2>
              <?php 
              
              $pesquisar = $_GET['pesquisar'];
              
              include('connect.php');

              $select = "SELECT * from obra_literaria
              WHERE titulo 
              LIKE '%$pesquisar%'
              OR autor
              LIKE '%$pesquisar%'";

              $res = $connect->query($select);
              $qtd = $res->num_rows;

              if($qtd>0){
              print "<br><p style='color: #0275d8;'>".$qtd." resultado(s) para '".$pesquisar."'</p>";
              
                while($row = $res->fetch_object()) {
                    
                    //biblioteca
                    $idbib = $row->biblioteca_idBIB;
                    $bib = "SELECT nomeBIB FROM biblioteca WHERE idBIB = $idbib";
                    $result = mysqli_query($connect, $bib);
                    $rows = mysqli_fetch_assoc($result); ?>   
          
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background: #d7d9da;">
                              <p><b><?php echo strtoupper($rows['nomeBIB'])?></b><p>
                              <p><b>Título: </b><?php echo $row->titulo ?><br><p>
                            </div>
                            <div class="panel-body">
                                <p class="card-text"><b>Autor(es): </b><?php echo $row->autor ?></p>
                                <p class="card-text"><b>Ano de Publicação: </b><?php echo $row->anoPub ?></p>
                                <p class="card-text"><b>Edição: </b><?php echo $row->edicao ?></p>
                                <p class="card-text"><b>Editora: </b><?php echo $row->editora ?></p>
                                <p class="card-text"><b>ISBN: </b><?php echo $row->isbn ?></p>
                                <p class="card-text"><b>Qtd. Cópias: </b><?php echo $row->qtCopias?></p>
                                  
                                <?php
                                //categoria
                                  $idcat = $row->categoria_obra_idCAT;
                                  $categ = "SELECT nomeCAT FROM categoria_obra WHERE idCAT = $idcat";
                                  $result2 = mysqli_query($connect, $categ);
                                  $row = mysqli_fetch_assoc($result2);?>
                                  <p class="card-text"><b>Categoria: </b><?php echo $row["nomeCAT"]?></p>
                              </div>
                          </div>
                  <?php }
              }
              else{
                print "<br><div class='alert alert-info'style='width: 600px' role='alert'>Nenhum resultado encontrado para '".$pesquisar."'.</div>";
                print "<div><br><br><br><br><br><br><br></div>";
              } ?>
            </div>
          </div>
        </div>
    </div>
    <script src='js/jquery.min.js' type='text/javascript'></script>
    <script src='js/popper.min.js'></script>
    <script src='js/bootstrap.min.js' type='text/javascript'></script>
    <script src='js/SidebarNav.min.js' type='text/javascript'></script>
    <script>
      $('.sidebar-menu').SidebarNav()
    </script>
  </body>
</html>



<footer>
    <p></p>
</footer>