<?php 
 include('perfil-bib/gerenciar-bib.php');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta content='IE=edge' http-equiv='X-UA-Compatible'/>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'/>
    <meta content='' name='author'/>
    <meta content='' name='description'/>
    <meta content='' name='keywords'/>
    <link href='css/bootstrap.min.css' media='all' rel='stylesheet' type='text/css'/>
    <link href='css/font-awesome.min.css' media='all' rel='stylesheet' type='text/css'/>
    <link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
    <link rel="shortcut icon" href="img/aco.ico"/>
    <script src="https://kit.fontawesome.com/bf4d0ad315.js" crossorigin="anonymous"></script>
    <title>Cadastrar-se - Biblioteca</title>
  </head>
  <body>
    <div class='wrapper'>
      <aside class="sidebar-left navbar navbar-inverse">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home.php" style="height: 75px;">
             <img src="img/aco-logo-white-50.png" alt="logo-book-with-mouse-clicker">
          </a>
        </div>
        <nav class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <li><a href="home.php"><i class="fas fa-home"></i> <span>INICIO</span></a></li>
            <li class="treeview">
              <a href="#">
              <i class="fa fa-edit"></i> <span>Cadastre-se</span>
              <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="cad-bib.php"><i class="fa fa-circle-o"></i> Biblioteca</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fas fa-sign-in-alt"></i> <span>Login</span>
              <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="adm-login.php"><i class="fa fa-circle-o"></i> Administrador</a></li>
                <li><a href="bib-login.php"><i class="fa fa-circle-o"></i> Biblioteca</a></li>
              </ul>
            </li>
            <li><a href="listar-bib.php"><i class="fas fa-book"></i> <span>Bibliotecas</span></a></li>
            <li>
                <form class="form-control-sm" method="GET" action="pesquisar.php">
                    <input class="form-control" type="text" name="pesquisar" placeholder="Pesquisar obra ou autor" aria-label="Search" style="width: 175px; margin-left: 1em; margin-top: 1em; font-size: 13px;">
                </form>
            </li>
          </ul>
        </nav>
    </aside>
    <div class='container'>
        <div class='row'>
            <div class='col-lg-9 col-lg-offset-1'>
                <form action="perfil-bib/gerenciar-bib.php" method="POST" data-toggle="validator">
                        <br>
                        <h2>CADASTRAR BIBLIOTECA</h2><br>
                        <div class="form-group">
                            <label>Nome da Biblioteca:</label>
                                    <input type="text" name="nomeBIB" class="form-control" value=""required autofocus>
                            <label>E-mail:</label>
                                    <input type="email" name="emailBIB" class="form-control" value=""required>
                            <label>Telefone:</label>
                                    <input type="number" name="telefone" class="form-control" value=""required>
                            <label>Endereço:</label>
                                    <input type="text" name="endereco" class="form-control" value=""required>
                            <label>Horário de funcionamento:</label>
                            <input type="text" name="horarioFuncionamento" class="form-control" value=""required>
                        </div>
                        <div class="form-group">
                            <br><span class="help-block">Agora, cadastre os dados de login.</span>
                            <label>Usuário:</label>
                            <input type="text" name="loginBIB" class="form-control" style="width: 190px" data-minlength="6" data-maxlength="6" required>
                        </div>
                        <div class="form-group">
                        <label>Senha:</label>
                            <input type="password" name="senha1" class="form-control" style="width: 190px" data-minlength="6" data-maxlength="6" required>
                            <span class="help-block">Confirme a senha:</span>
                            <input type="password" name="senha2" class="form-control" style="width: 190px" data-minlength="6" data-maxlength="6" required>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" type="submit" name="save">Cadastrar</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
    <script src='js/jquery.min.js' type='text/javascript'></script>
    <script src='js/bootstrap.min.js' type='text/javascript'></script>
    <script src='js/SidebarNav.min.js' type='text/javascript'></script>
    <script>
      $('.sidebar-menu').SidebarNav()
    </script>
  </body>
</html>




<footer>
    <p></p>
</footer>
    

