<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta content='IE=edge' http-equiv='X-UA-Compatible'/>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'/>
    <meta content='' name='author'/>
    <meta content='' name='description'/>
    <meta content='' name='keywords'/>
    <link href='css/bootstrap.min.css' media='all' rel='stylesheet' type='text/css'/>
    <link href='css/font-awesome.min.css' media='all' rel='stylesheet' type='text/css'/>
    <link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
    <link rel="shortcut icon" href="img/aco.ico"/>
    <script src="https://kit.fontawesome.com/bf4d0ad315.js" crossorigin="anonymous"></script>
    <title>ACO - Perfil de Biblioteca</title>
    <style>
        h1{color: #222d32;}
    </style>
  </head>
  <body>
    <div class='wrapper'>
      <aside class="sidebar-left navbar navbar-inverse">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home.php" style="height: 75px;">
             <img src="img/aco-logo-white-50.png" alt="logo-book-with-mouse-clicker">
          </a>
        </div>
        <nav class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <li><a href="home.php"><i class="fas fa-home"></i> <span>INICIO</span></a></li>
            <li class="treeview">
              <a href="#">
              <i class="fa fa-edit"></i> <span>Cadastre-se</span>
              <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="bib-cadastrar.php"><i class="fa fa-circle-o"></i> Biblioteca</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fas fa-sign-in-alt"></i> <span>Login</span>
              <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="adm-login.php"><i class="fa fa-circle-o"></i> Administrador</a></li>
                <li><a href="bib-login.php"><i class="fa fa-circle-o"></i> Biblioteca</a></li>
              </ul>
            </li>
            <li><a href="listar-bib.php"><i class="fas fa-book"></i> <span>Bibliotecas</span></a></li>
            <li>
            <?php include('connect.php');
                if (isset($_GET['profile'])) {
                    $id = $_GET['profile']; ?>
                <form class="form-control-sm" method="GET" action="pesquisar-obra.php">
                    <input type="hidden" name="perfil-bib" value="<?php echo $id ?>">
                    <input class="form-control" type="text" name="pesquisar-obra" placeholder="Pesquisar neste acervo" aria-label="Search" style="width: 175px; margin-left: 1em; margin-top: 1em; font-size: 13px;">
                </form>
            </li>
          </ul>
        </nav>
      </aside>
      <div class="container">
        <div class="row">
          <div class="col-lg-9">
          <?php 
          $bib = "SELECT nomeBIB FROM biblioteca WHERE idBIB = $id";
                  $result = mysqli_query($connect, $bib);
                  $rows = mysqli_fetch_assoc($result); ?>
                  <br><h1>ACERVO</h1>
                  <h2><?php echo strtoupper($rows['nomeBIB']) ?></h2>
            
            <?php    
            $query = "SELECT  ob.*, bb.* 
                      FROM obra_literaria AS ob
                      INNER JOIN biblioteca AS bb
                      ON ob.biblioteca_idBIB = bb.idBIB
                      WHERE bb.idBIB 
                      LIKE $id";

                      $res = $connect->query($query);
                      $qtd = $res->num_rows;
       
                }
                if($qtd>0){
                    
                    echo "<i class='fas fa-book fa-sm'></i> ".$qtd." Obra(s)<br><br><br>";
                    
                    while($row = $res->fetch_object()) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" style="background: #d7d9da;">
                                    <p><b>Título: </b><?php echo $row->titulo ?><br><p>
                                </div>
                                <div class="panel-body">
                                    <p class="card-text"><b>Autor: </b><?php echo $row->autor ?></p>
                                    <p class="card-text"><b>Ano de Publicação: </b><?php echo $row->anoPub ?></p>
                                    <p class="card-text"><b>Edição: </b><?php echo $row->edicao ?></p>
                                    <p class="card-text"><b>Editora: </b><?php echo $row->editora ?></p>
                                    <p class="card-text"><b>ISBN: </b><?php echo $row->isbn ?></p>
                                    <p class="card-text"><b>Qtd. Cópias: </b><?php echo $row->qtCopias?></p>
                                        
                                    <?php
                                    //categoria
                                        $idcat = $row->categoria_obra_idCAT;
                                        $categ = "SELECT nomeCAT FROM categoria_obra WHERE idCAT = $idcat";
                                        $result2 = mysqli_query($connect, $categ);
                                        $row = mysqli_fetch_assoc($result2);?>
                                        <p class="card-text"><b>Categoria: </b><?php echo $row["nomeCAT"]?></p>
                                    </div>
                                </div>
                        <?php }
                    }
                    else{
                      print "<br><div class='alert alert-info'style='width: 600px' role='alert'>Nenhuma obra neste acervo.</div>";
                      print "<div><br><br><br><br><br><br><br></div>";
                    } ?>
            </div>
        </div>
    </div>
    <script src='js/jquery.min.js' type='text/javascript'></script>
    <script src='js/popper.min.js'></script>
    <script src='js/bootstrap.min.js' type='text/javascript'></script>
    <script src='js/SidebarNav.min.js' type='text/javascript'></script>
    <script>
      $('.sidebar-menu').SidebarNav()
    </script>
  </body>
</html>



<footer>
    <p></p>
</footer>